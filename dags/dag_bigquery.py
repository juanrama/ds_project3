from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.providers.google.cloud.hooks.bigquery import BigQueryHook
from airflow.hooks.postgres_hook import PostgresHook
from airflow.utils.dates import days_ago
from sqlalchemy import create_engine
from google.cloud import bigquery
import pandas as pd
from datetime import datetime, date, timedelta
import logging

default_args = {
    'owner': 'airflow',
    'start_date': days_ago(1),
    'retries': 3,
    'retry_delay': timedelta(minutes=5),
}

def serialize_data(data):
    """Convert non-serializable objects to serializable format for JSON."""
    if isinstance(data, list):
        return [serialize_data(item) for item in data]
    elif isinstance(data, dict):
        return {key: serialize_data(value) for key, value in data.items()}
    elif isinstance(data, datetime):
        return data.isoformat()
    elif isinstance(data, date):
        return data.isoformat()
    elif isinstance(data, memoryview):
        return data.tobytes().decode('utf-8')
    return data

def extract_data_from_postgresql(src_table, **kwargs):
    try:
        pg_hook = PostgresHook(postgres_conn_id='postgres_conn')
        connection = pg_hook.get_uri()
        engine = create_engine(connection)
        
        schema = 'raw_data'
        query = f'SELECT * FROM {schema}.{src_table}'
        records = pd.read_sql(query, engine)
        
        serialized_records = serialize_data(records.to_dict(orient='records'))
        
        kwargs['ti'].xcom_push(key=f'{src_table}_data', value=serialized_records)
        logging.info(f'Successfully extracted data from table {src_table}')
    except Exception as e:
        logging.error(f'Error extracting data from table {src_table}: {str(e)}')
        raise

def load_data_to_bigquery(src_table, dest_table, **kwargs):
    try:
        ti = kwargs['ti']
        extracted_data = ti.xcom_pull(key=f'{src_table}_data', task_ids=f'extract_{src_table}')
        
        bq_hook = BigQueryHook(gcp_conn_id='bigquery_conn')
        client = bq_hook.get_client()
        
        table_id = f'ratingup-405505.ds_sklola_dataset.{dest_table}'
        
        job_config = bigquery.LoadJobConfig(
            write_disposition=bigquery.WriteDisposition.WRITE_TRUNCATE,
            source_format=bigquery.SourceFormat.NEWLINE_DELIMITED_JSON,
        )
        
        job = client.load_table_from_json(extracted_data, table_id, job_config=job_config)
        job.result()
        
        logging.info(f'Successfully loaded data to BigQuery table {dest_table}')
    except Exception as e:
        logging.error(f'Error loading data to BigQuery table {dest_table}: {str(e)}')
        raise

def success_notification(dest_table, **kwargs):
    logging.info(f'Task for {dest_table} has been successfully completed.')

tables = {
    'regions': 'dim_regions', 
    'categories': 'dim_categories',
    'suppliers': 'dim_suppliers', 
    'shippers': 'dim_shippers', 
    'territories': 'dim_territories',
    'employee_territories': 'dim_emp_territories', 
    'employees': 'dim_employees', 
    'customers': 'dim_customers', 
    'products': 'dim_products',
    'orders': 'fact_orders', 
    'order_details': 'fact_order_details'
}

with DAG(
    dag_id='extract_load_dag_x20',
    default_args=default_args,
    schedule_interval=None,
) as dag:

    start = DummyOperator(
        task_id='start',
    )

    end = DummyOperator(
        task_id='end',
    )

    for src_table, dest_table in tables.items():
        extract_task = PythonOperator(
            task_id=f'extract_{src_table}',
            python_callable=extract_data_from_postgresql,
            op_kwargs={'src_table': src_table},
            provide_context=True,
        )
        
        load_task = PythonOperator(
            task_id=f'load_{src_table}',
            python_callable=load_data_to_bigquery,
            op_kwargs={'src_table': src_table, 'dest_table': dest_table},
            provide_context=True,
        )
        
        notify_task = PythonOperator(
            task_id=f'notify_{dest_table}',
            python_callable=success_notification,
            op_kwargs={'dest_table': dest_table},
            provide_context=True,
        )
        
        start >> extract_task >> load_task >> notify_task >> end
